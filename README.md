# ohos-stackblur

**本项目是基于开源项目stackblur进行ohos化的移植和开发的，可以通过项目标签以及github地址（https://github.com/kikoso/android-stackblur ）追踪到原项目版本**

#### 项目介绍

- 项目名称：ohos-stackblur图片模糊工具库
- 所属系列：ohos的第三方组件适配移植
- 功能：提供一套图片模糊框架，可对用户指定图片施加模糊效果
- 项目移植状态：完成
- 调用差异：无
- 项目作者和维护人：hihope
- 联系方式：hihope@hoperun.com
- 原项目Doc地址：https://github.com/kikoso/android-stackblur
- 原项目基线版本：未发布Release版本
- 编程语言：Java
- 外部库依赖：无
stack_blur1

#### 效果展示

<img src="gif/stack_blur1.gif"/>

<img src="gif/stack_blur2.gif"/>

#### 安装教程

方法1.

1. 下载三方库har包StackBlur.har。
2. 启动 DevEco Studio，将下载的har包，导入工程目录“entry->libs”下。
3. 在module级别下的build.gradle文件中添加依赖，在dependences标签中增加对libs目录下har包的引用。

```
repositories {
    flatDir { dirs 'libs' }
}
dependencies {
    implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
    implementation(name: 'StackBlur', ext: 'har')
	……
}
```

4. 如需使用native算法库，需编译stackBlur源码，将build/intermediates/cmake/debug/obj路径下的arm64-v8a文件夹拷贝到添加到entry模块的libs/文件夹下

方法2.

1. 在工程的build.gradle的allprojects中，添加HAR所在的Maven仓地址

```
repositories {
    maven {
        url 'http://106.15.92.248:8081/repository/Releases/'
    }
}
```

2. 在应用模块的build.gradle的dependencies闭包中，添加如下代码:

```
dependencies {
    implementation 'com.enrique.ohos:stackblur:1.0.1'
}
```

#### 使用说明

初始化StackBlurManager并加载图片资源:
```java
    stackBlurManager = new StackBlurManager(getPixelMapFromRawfile(this, "image_transparency.png"));
```
设置模糊半径:
```java
    stackBlurManager.process(progress * 5);
```
获取模糊处理后的图片并设置给Image控件或其他UI组件:
```java
    image.setPixelMap(stackBlurManager.returnBlurredImage());
```
如果想要使用native代码则需要调用:
```java
    stackBlurManager.processNatively(progress * 5);
```

#### 版本迭代

- v1.0.1

#### 版权和许可信息

- Apache License, Version 2.0
