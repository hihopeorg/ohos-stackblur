package com.enrique.stackblur;

import ohos.app.Context;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;

import static com.enrique.stackblur.JavaBlurProcessTest.context;
import static org.junit.Assert.*;
public class StackBlurManagerTest {
    private static final String IMAGE_NAME_OPAQUE = "wallpaper_car.png";
    @Test
    public void process() {
        StackBlurManager stackBlurManager = new StackBlurManager(getPixelMapFromRawfile(context,IMAGE_NAME_OPAQUE));
        assertEquals(true,stackBlurManager.process(1) instanceof PixelMap);

    }

    @Test
    public void returnBlurredImage() {
        StackBlurManager stackBlurManager = new StackBlurManager(getPixelMapFromRawfile(context,IMAGE_NAME_OPAQUE));
        stackBlurManager.process(1);
        assertEquals(true,stackBlurManager.returnBlurredImage() instanceof PixelMap);
    }

    @Test
    public void getImage() {
        StackBlurManager stackBlurManager = new StackBlurManager(getPixelMapFromRawfile(context,IMAGE_NAME_OPAQUE));
        stackBlurManager.process(1);
        assertEquals(true,stackBlurManager.getImage() instanceof PixelMap);
    }

    @Test
    public void processNatively() {
        StackBlurManager stackBlurManager = new StackBlurManager(getPixelMapFromRawfile(context,IMAGE_NAME_OPAQUE));
        assertEquals(true,stackBlurManager.processNatively(1) instanceof PixelMap);
    }

    private PixelMap getPixelMapFromRawfile(Context context, String strName) {
        InputStream istr;
        PixelMap bitmap = null;
        try {
            istr = context.getResourceManager().getRawFileEntry("resources/rawfile/" + strName).openRawFile();
            ImageSource imageSource = ImageSource.create(istr, new ImageSource.SourceOptions());
            bitmap = imageSource.createPixelmap(new ImageSource.DecodingOptions());
        } catch (IOException e) {
            return null;
        }
        return bitmap;
    }
}