package com.enrique.stackblur;

import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.aafwk.ability.delegation.IAbilityDelegator;
import ohos.app.Context;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;

import static org.junit.Assert.*;
public class JavaBlurProcessTest {
    public static Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
    private static final String IMAGE_NAME_OPAQUE = "wallpaper_car.png";
    @Test
    public void blur() {
        JavaBlurProcess javaBlurProcess = new JavaBlurProcess();
        assertEquals(true,
                javaBlurProcess.blur(getPixelMapFromRawfile(context,IMAGE_NAME_OPAQUE),1)
                        instanceof
                        PixelMap
        );
    }

    private PixelMap getPixelMapFromRawfile(Context context, String strName) {
        InputStream istr;
        PixelMap bitmap = null;
        try {
            istr = context.getResourceManager().getRawFileEntry("resources/rawfile/" + strName).openRawFile();
            ImageSource imageSource = ImageSource.create(istr, new ImageSource.SourceOptions());
            bitmap = imageSource.createPixelmap(new ImageSource.DecodingOptions());
        } catch (IOException e) {
            return null;
        }
        return bitmap;
    }
}