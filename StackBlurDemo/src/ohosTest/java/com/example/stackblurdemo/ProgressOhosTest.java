package com.example.stackblurdemo;

import com.enrique.stackblur.StackBlurManager;
import com.example.stackblurdemo.BenchmarkAbility;
import com.example.stackblurdemo.MainAbility;
import com.example.stackblurdemo.ResourceTable;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.agp.components.*;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.media.image.PixelMap;
import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ProgressOhosTest {
    private static Ability ability = EventHelper.startAbility(MainAbility.class);
    private static final HiLogLabel LABEL = new HiLogLabel(HiLog.LOG_APP, 0x00102, "StepsView");

    @Test
    public void test01() {
        stackBlurManager();
    }

    @Test
    public void test02() {
        ToggleButton toggleButton = (ToggleButton) ability.findComponentById(ResourceTable.Id_toggleButton);
        AbilityDelegatorRegistry.getAbilityDelegator().triggerClickEvent(ability, toggleButton);
        threadSleep(3000);
        stackBlurManager();
    }

    @Test
    public void test03() {
        RadioContainer radioContainer = (RadioContainer) ability.findComponentById(ResourceTable.Id_typeSelectSpinner);
        RadioButton radioButton = (RadioButton) radioContainer.getComponentAt(1);
        AbilityDelegatorRegistry.getAbilityDelegator().triggerClickEvent(ability, radioButton);
        threadSleep(3000);
        stackBlurManager();
    }
    @Test
    public void test04(){
        RadioContainer radioContainer = (RadioContainer) ability.findComponentById(ResourceTable.Id_typeSelectSpinner);
        RadioButton radioButton = (RadioButton) radioContainer.getComponentAt(1);
        AbilityDelegatorRegistry.getAbilityDelegator().triggerClickEvent(ability, radioButton);
        threadSleep(3000);
        ToggleButton toggleButton = (ToggleButton) ability.findComponentById(ResourceTable.Id_toggleButton);
        AbilityDelegatorRegistry.getAbilityDelegator().triggerClickEvent(ability, toggleButton);
        threadSleep(3000);
        stackBlurManager();
    }
    @Test
    public void test05(){
        Button button=(Button)ability.findComponentById(ResourceTable.Id_benchmark);
        AbilityDelegatorRegistry.getAbilityDelegator().triggerClickEvent(ability,button);
        threadSleep(3000);
        try {
            Runtime.getRuntime().exec("input tap 200 950");
        } catch (IOException e) {
            e.printStackTrace();
        }
        threadSleep(1000);
        try {
            Runtime.getRuntime().exec("input tap 400 950");
        } catch (IOException e) {
            e.printStackTrace();
        }
        threadSleep(3000);
        //java.ms
        Text text1 = (Text)AbilityDelegatorRegistry.getAbilityDelegator().getCurrentTopAbility().findComponentById(ResourceTable.Id_detail_java);
        String text01 = text1.getText();
        String regEx="[^0-9]";
        Pattern p=Pattern.compile(regEx);
        Matcher m = p.matcher(text01);
        String t = m.replaceAll("").trim();
        int ms1=Integer.parseInt(t);
        threadSleep(2000);
        HiLog.warn(LABEL,"内容"+ text01);
        //native.ms
        Text text3 = (Text)AbilityDelegatorRegistry.getAbilityDelegator().getCurrentTopAbility().findComponentById(ResourceTable.Id_detail_native);
        String text03 = text3.getText();
        String regEx02="[^0-9]";
        Pattern p02=Pattern.compile(regEx02);
        Matcher m02 = p02.matcher(text03);
        String t02 = m02.replaceAll("").trim();
        int ms3=Integer.parseInt(t02);
        HiLog.warn(LABEL,"内容"+ text03);
        try {
            Runtime.getRuntime().exec("input tap 600 950");
        } catch (IOException e) {
            e.printStackTrace();
        }
        threadSleep(3000);
        //java.ms
        Text text2 = (Text)AbilityDelegatorRegistry.getAbilityDelegator().getCurrentTopAbility().findComponentById(ResourceTable.Id_detail_java);
        String text02 = text2.getText();
        String regEx0="[^0-9]";
        Pattern p0=Pattern.compile(regEx0);
        Matcher m0 = p0.matcher(text02);
        String t0 = m0.replaceAll("").trim();
        int ms2=Integer.parseInt(t0);
        HiLog.warn(LABEL,"内容"+text02);
        Assert.assertTrue("java平均时间大于40ms",ms1+ms2<80);
        //native.ms
        Text text4 = (Text)AbilityDelegatorRegistry.getAbilityDelegator().getCurrentTopAbility().findComponentById(ResourceTable.Id_detail_native);
        String text04 = text4.getText();
        String regEx03="[^0-9]";
        Pattern p03=Pattern.compile(regEx03);
        Matcher m03 = p03.matcher(text04);
        String t03 = m03.replaceAll("").trim();
        int ms4=Integer.parseInt(t03);
        HiLog.warn(LABEL,"内容"+text04);
        Assert.assertTrue("native平均时间大于40ms",ms3+ms4<300);
    }
    public void threadSleep(int x) {
        try {
            Thread.sleep(x);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    public void stackBlurManager(){
        threadSleep(2000);
        try {
            Runtime.getRuntime().exec("input tap 400 300");
        } catch (IOException e) {
            e.printStackTrace();
        }
        threadSleep(2000);
        StackBlurManager stackBlurManager = null;
        try {
            Field field = MainAbility.class.getDeclaredField("_stackBlurManager");
            field.setAccessible(true);
            stackBlurManager = (StackBlurManager) field.get(ability);
        } catch (NoSuchFieldException e) {
            throw new RuntimeException("没有找到判断");
        } catch (IllegalAccessException e) {
            throw new RuntimeException("非法访问");
        }
        PixelMap pixelMap = stackBlurManager.returnBlurredImage();
        try {
            Runtime.getRuntime().exec("input tap 700 300");
        } catch (IOException e) {
            e.printStackTrace();
        }

        threadSleep(2000);
        StackBlurManager stackBlurManager1 = null;
        try {
            Field field = MainAbility.class.getDeclaredField("_stackBlurManager");
            field.setAccessible(true);
            stackBlurManager1 = (StackBlurManager) field.get(ability);
        } catch (NoSuchFieldException e) {
            throw new RuntimeException("没有找到相反判断");
        } catch (IllegalAccessException e) {
            throw new RuntimeException("非法访问");
        }
        PixelMap pixelMap1 = stackBlurManager1.returnBlurredImage();
        boolean sameImage = pixelMap1.isSameImage(pixelMap);
        HiLog.warn(LABEL, "image" + sameImage);
        Assert.assertTrue("图片没有变化", sameImage == false);
    }
}
