package com.example.stackblurdemo;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.AttrHelper;
import ohos.agp.components.Image;
import ohos.agp.components.ProgressBar;
import ohos.agp.components.Slider;
import ohos.agp.components.Text;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.render.PixelMapHolder;
import ohos.agp.render.Texture;
import ohos.agp.utils.Color;
import ohos.agp.utils.Point;
import ohos.global.resource.NotExistException;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;
import ohos.miscservices.timeutility.Time;

import com.enrique.stackblur.StackBlurManager;

import java.io.IOException;

public class BenchmarkAbility extends Ability {

    ProgressBar _javaProgressbar;
    ProgressBar _nativeProgressbar;
    Text _javaDetail;
    Text _nativeDetail;
    Slider _blurAmt;
    Text _blurAmtText;
    Image _resultImage;

    private BenchmarkTask _benchmarkTask;


    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_benchmark);
        _javaProgressbar=findViewById(ResourceTable.Id_progress_java);
        _nativeProgressbar=findViewById(ResourceTable.Id_progress_native);
        _javaDetail=findViewById(ResourceTable.Id_detail_java);
        _nativeDetail=findViewById(ResourceTable.Id_detail_native);
        _blurAmt=findViewById(ResourceTable.Id_blur_amount);
        _blurAmtText=findViewById(ResourceTable.Id_blur_amount_detail);
        _resultImage=findViewById(ResourceTable.Id_result_img);

        _blurAmt.setValueChangedListener(new Slider.ValueChangedListener() {
            @Override
            public void onProgressUpdated(Slider slider, int progress, boolean fromUser) {
                _blurAmtText.setText(progress + " px");
                if(_benchmarkTask != null) {
                    _benchmarkTask.cancel(true);
                }
                _benchmarkTask = new BenchmarkTask();
                _benchmarkTask.execute(progress);
            }

            @Override
            public void onTouchStart(Slider slider) {

            }

            @Override
            public void onTouchEnd(Slider slider) {

            }
        });
    }

    private class BenchmarkTask extends AsyncTask<Integer, BlurBenchmarkResult, PixelMap> {
        private int max = Integer.MIN_VALUE;
        private PixelMap outBitmap;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            _javaProgressbar.setProgressValue(-1);
            _nativeProgressbar.setProgressValue(-1);

            _javaProgressbar.setIndeterminate(true);
            _nativeProgressbar.setIndeterminate(true);

            _javaDetail.setText("");
            _nativeDetail.setText("");
        }

        @Override
        protected PixelMap doInBackground(Integer... params) {
            if(params.length != 1 || params[0] == null)
                throw new IllegalArgumentException("Pass only 1 Integer to BenchmarkTask");
            int blurAmount = params[0];
            PixelMap inBitmap = null;
            PixelMap blurredBitmap;
            Paint paint = new Paint();

            try {
                ImageSource imageSource = ImageSource.create(getResourceManager().getResource(ResourceTable.Media_image_transparency), null);
                inBitmap = imageSource.createPixelmap(new ImageSource.DecodingOptions());
            } catch (IOException e) {
                e.printStackTrace();
            } catch (NotExistException e) {
                e.printStackTrace();
            }
            if (inBitmap == null) {
                return null;
            }

            PixelMap.InitializationOptions initializationOptions = new PixelMap.InitializationOptions();
            initializationOptions.size = inBitmap.getImageInfo().size;
            initializationOptions.useSourceIfMatch = false;
            initializationOptions.editable = true;
            outBitmap = PixelMap.create(inBitmap, initializationOptions);
            Canvas canvas = new Canvas(new Texture(outBitmap));

            long time;
            StackBlurManager blurManager = new StackBlurManager(inBitmap);

            BlurBenchmarkResult result;

            // Java
            time = Time.getRealTime();
            blurredBitmap = blurManager.process(blurAmount);
            result = new BlurBenchmarkResult("Java", (int) (Time.getRealTime() - time));
            canvas.save();
            canvas.clipRect(0, 0, outBitmap.getImageInfo().size.width / 3, outBitmap.getImageInfo().size.height);
            canvas.drawPixelMapHolder(new PixelMapHolder(blurredBitmap), 0, 0, paint);
            canvas.restore();
            publishProgress(result);
            blurredBitmap.release();

            if(isCancelled())
                return outBitmap;

            // Native

            time = Time.getRealTime();
            blurredBitmap = blurManager.processNatively(blurAmount);
            result = new BlurBenchmarkResult("Native", (int) (Time.getRealTime() - time));
            canvas.save();
            canvas.clipRect(outBitmap.getImageInfo().size.width / 3, 0, 2 * outBitmap.getImageInfo().size.width / 3, outBitmap.getImageInfo().size.height);
            canvas.drawPixelMapHolder(new PixelMapHolder(blurredBitmap), 0, 0, paint);
            canvas.restore();
            publishProgress(result);
            blurredBitmap.release();

            if(isCancelled())
                return outBitmap;

            // Origin
            canvas.save();
            canvas.clipRect(2 * outBitmap.getImageInfo().size.width / 3, 0, outBitmap.getImageInfo().size.width, outBitmap.getImageInfo().size.height);
            canvas.drawPixelMapHolder(new PixelMapHolder(inBitmap), 0, 0, paint);
            canvas.restore();

            Paint linePaint = new Paint();
            linePaint.setAntiAlias(true);
            linePaint.setStyle(Paint.Style.STROKE_STYLE);
            linePaint.setStrokeWidth(AttrHelper.vp2px(1, getContext()));
            linePaint.setColor(Color.BLACK);
            canvas.drawLine(new Point(outBitmap.getImageInfo().size.width / 3, 0), new Point(outBitmap.getImageInfo().size.width / 3, outBitmap.getImageInfo().size.height), linePaint);
            canvas.drawLine(new Point(2 * outBitmap.getImageInfo().size.width / 3, 0), new Point(2 * outBitmap.getImageInfo().size.width / 3, outBitmap.getImageInfo().size.height), linePaint);

            return outBitmap;
        }

        @Override
        protected void onPostExecute(PixelMap result) {
            super.onPostExecute(result);
            _resultImage.setPixelMap(result);
        }

        @Override protected void onProgressUpdate(BlurBenchmarkResult... values) {
            super.onProgressUpdate(values);
            _resultImage.setPixelMap(outBitmap);
            for (BlurBenchmarkResult benchmark : values) {
                if(benchmark == null)
                    continue;
                if(benchmark._processingMillis > max) {
                    max = benchmark._processingMillis;
                    if(!_javaProgressbar.isIndeterminate())
                        _javaProgressbar.setMaxValue(max);
                    if(!_nativeProgressbar.isIndeterminate())
                        _nativeProgressbar.setMaxValue(max);
                }
                if("Java".equals(benchmark._processingType)) {
                    _javaProgressbar.setIndeterminate(false);
                    _javaProgressbar.setMaxValue(max);
                    _javaProgressbar.setProgressValue(benchmark._processingMillis);
                    _javaDetail.setText(benchmark._processingMillis + " ms");
                }
                else if("Native".equals(benchmark._processingType)) {
                    _nativeProgressbar.setIndeterminate(false);
                    _nativeProgressbar.setMaxValue(max);
                    _nativeProgressbar.setProgressValue(benchmark._processingMillis);
                    _nativeDetail.setText(benchmark._processingMillis + " ms");
                }
            }
        }

    }

    private static class BlurBenchmarkResult {
        public final int _processingMillis;
        public final String _processingType;

        private BlurBenchmarkResult(String processingType, int processingMillis) {
            _processingType = processingType;
            _processingMillis = processingMillis;
        }
    }

    private <T> T findViewById(int resId) {
        return (T)findComponentById(resId);
    }
}
