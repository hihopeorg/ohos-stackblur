package com.example.stackblurdemo;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.AbsButton;
import ohos.agp.components.Component;
import ohos.agp.components.Image;
import ohos.agp.components.RadioContainer;
import ohos.agp.components.Slider;
import ohos.agp.components.ToggleButton;
import ohos.app.Context;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;

import com.enrique.stackblur.StackBlurManager;

import java.io.IOException;
import java.io.InputStream;

public class MainAbility extends Ability {

    private static final String IMAGE_NAME_OPAQUE = "wallpaper_car.png";
    private static final String IMAGE_NAME_TRANSPARENT = "image_transparency.png";
    Image _imageView;
    Slider _seekBar;
    ToggleButton _toggleButton;
    RadioContainer _typeSelectSpinner;

    private StackBlurManager _stackBlurManager;

    private String imageToAnalyze = IMAGE_NAME_OPAQUE;

    private int blurMode;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        _imageView = (Image)findComponentById(ResourceTable.Id_imageView);
        _seekBar = (Slider)findComponentById(ResourceTable.Id_blur_amount);
        _toggleButton = (ToggleButton)findComponentById(ResourceTable.Id_toggleButton);
        _typeSelectSpinner = (RadioContainer)findComponentById(ResourceTable.Id_typeSelectSpinner);
        _stackBlurManager = new StackBlurManager(getPixelMapFromRawfile(this, imageToAnalyze));

        _seekBar.setValueChangedListener(new Slider.ValueChangedListener() {
            @Override
            public void onProgressUpdated(Slider slider, int i, boolean b) {
                onBlur();
            }

            @Override
            public void onTouchStart(Slider slider) {

            }

            @Override
            public void onTouchEnd(Slider slider) {

            }
        });

        _toggleButton.setCheckedStateChangedListener(new AbsButton.CheckedStateChangedListener() {

            @Override
            public void onCheckedChanged(AbsButton absButton, boolean isChecked) {
                if (isChecked) {
                    imageToAnalyze = IMAGE_NAME_TRANSPARENT;
                    _stackBlurManager = new StackBlurManager(getPixelMapFromRawfile(getApplicationContext(), imageToAnalyze));
                    onBlur();
                } else {
                    imageToAnalyze = IMAGE_NAME_OPAQUE;
                    _stackBlurManager = new StackBlurManager(getPixelMapFromRawfile(getApplicationContext(), imageToAnalyze));
                    onBlur();
                }
            }
        });

        _typeSelectSpinner.setMarkChangedListener(new RadioContainer.CheckedStateChangedListener() {
            @Override
            public void onCheckedChanged(RadioContainer radioContainer, int position) {
                setBlurMode(position);
            }
        });

        findComponentById(ResourceTable.Id_benchmark).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                Intent intent = new Intent();
                Operation operation = new Intent.OperationBuilder()
                        .withBundleName(getAbilityPackageContext().getBundleName())
                        .withAbilityName(BenchmarkAbility.class.getCanonicalName())
                        .build();
                intent.setOperation(operation);
                startAbility(intent);
            }
        });
    }

    private PixelMap getPixelMapFromRawfile(Context context, String strName) {
        InputStream istr;
        PixelMap bitmap = null;
        try {
            istr = context.getResourceManager().getRawFileEntry("resources/rawfile/" + strName).openRawFile();
            ImageSource imageSource = ImageSource.create(istr, new ImageSource.SourceOptions());
            bitmap = imageSource.createPixelmap(new ImageSource.DecodingOptions());
        } catch (IOException e) {
            return null;
        }
        return bitmap;
    }

    public void setBlurMode(int mode) {
        this.blurMode = mode;
        onBlur();
    }

    private void onBlur() {
        int radius = _seekBar.getProgress() * 5;
        switch(blurMode) {
            case 0:
                _imageView.setPixelMap( _stackBlurManager.process(radius) );
                break;
            case 1:
                _imageView.setPixelMap( _stackBlurManager.processNatively(radius) );
                break;
        }
    }
}
